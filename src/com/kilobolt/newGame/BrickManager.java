package com.kilobolt.newGame;

import java.util.ArrayList;
import java.util.Random;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class BrickManager {
	
	ArrayList<Brick> list;
	Random rand;
	
	
	BrickManager()
	{
		list = new ArrayList<Brick>();
		rand = new Random();
		list.add(new Brick(0, 350,100 ,20 , 0));
		list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,150,20, 1));
		list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,200,20, 2));
	
		
		
	}
	
	
	public void Update(float deltaTime, Input input)
	{	
		
		if(!background.reverseBTDown)
		{	
		
		int type = rand.nextInt(6);
		if(type == 0)
		{
			list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,100,20, 0));
		}
		else if(type == 1)
		{
			list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,150,20, 1));
		}
		else if(type == 2)
		{
			list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,200,20, 2));
		}
		else if(type == 3)
		{
			list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,250,20, 3));
		}
		else if(type == 4)
		{
			list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,300,20, 4));
		}
		else if(type == 5)
		{
			list.add(new Brick(list.get(list.size()-1).posX + list.get(list.size()-1).width + 80, 350,350,20, 5));
		}
		
			for(int i = 0; i<list.size(); i++)
			{
				list.get(i).Update(deltaTime, input);
			}
		}
		else
		{
			for(int i = 0; i<list.size(); i++)
			{
				list.get(i).Update(deltaTime, input);
			}
			
		}
		
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		for(int i = 0; i<list.size();i++)
		{
			list.get(i).Paint(deltaTime, g);
		}
	}


}
