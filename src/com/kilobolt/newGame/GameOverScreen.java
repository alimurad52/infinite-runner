package com.kilobolt.newGame;

import android.graphics.Color;
import android.graphics.Paint;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class GameOverScreen extends Screen {

	
	
	public GameOverScreen(Game game) 
	{
		super(game);	
		
	}

	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		
				
		if(input.isTouchDown(0))
		{
			Player.getInstance().score = 0;
			game.setScreen(new MenuScreen(game));
		}
	}

	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		g.drawImage(Assets.gameOver, 0, 0);
		
		Paint p = new Paint();
		p.setColor(Color.RED);
		p.setTextSize(30);
		g.drawString("Score: " +  Integer.toString(Player.getInstance().score), 330, 50, p);
		
		
	}
	
	
	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}
}
