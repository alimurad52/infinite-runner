package com.kilobolt.newGame;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class Player 
{
	public int posX, posY;
	int gravity;
	public int width, height;
	private static Player instance;
	public boolean IsOnGround = false;
	private int upSpeed;
	int index;
	float reverseTime;
	public boolean ISFalling = false;
	
	public static int score;
	public static boolean isDead = false;
	float t = 0;
	

	public static Player getInstance()
	{
		return instance;
	}
	
	public Player(int pX, int pY)
	{
		instance = this;
		posX = pX;
		posY = pY;
		gravity = 10;
		width = 20;
		height = 20;
		upSpeed = 0;
		reverseTime = 0;
	}
	
	public void Update(float deltaTime, Input input)
	{
		reverseTime += deltaTime/60;
		
		
		if(!background.reverseBTDown)
		{
			
			score += 1;
			
		posY += gravity;
		posY += upSpeed;
		
		
		if(input.isTouchDown(0))
		{
			if(IsOnGround && !ISFalling )
			{
				Assets.jump.play(1.0f);
				upSpeed = -(gravity + 15);
				IsOnGround = false;
			}	
		}
		
		
		if(upSpeed < 0)
		{
			upSpeed += 1;
		}
		
		if(reverseTime < 5)
		{
			Recording.playerPos.add(new Point(posX, posY));
		}
		else
		{
			Recording.playerPos.add(new Point(posX, posY));
			Recording.playerPos.remove(0);
		}
		
		index = Recording.playerPos.size() - 1;
		}
		else
		{
			if(index > 0)
			{
				score -= 1;
			posX = Recording.playerPos.get(index).x;
			posY = Recording.playerPos.get(index).y;
			index--;
			}
		}
		
		if(posY > 370)
			ISFalling = true;
		else
			ISFalling = false;
		
		
		if(posY > 450)
		{
			t += deltaTime/60;
			if(t > 3)
			{
				isDead = true;
				t = 0;
			}
			
		}
		
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
	
		Paint p = new Paint();
		p.setColor(Color.RED);
		p.setTextSize(30);
		g.drawImage(Assets.player, posX, posY);
		g.drawString("Score: " +  Integer.toString(score), 10, 30, p);
		g.drawImage(Assets.lava, 0, 450);
	}
	

}
