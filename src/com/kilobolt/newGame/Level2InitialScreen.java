package com.kilobolt.newGame;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class Level2InitialScreen extends Screen {

	public Level2InitialScreen(Game game) 
	{
		super(game);		
	}

	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		if(input.isTouchDown(0))
		{
			game.setScreen(new Level2Gameplay(game));
		}
	}

	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		g.drawImage(Assets.level2, 0, 0);
	}
	
	
	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}
}
