package com.kilobolt.newGame;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Screen;
import com.kilobolt.framework.Graphics.ImageFormat;
import com.kilobolt.framework.implementation.AndroidGame;


public class SampleGame extends AndroidGame{
	

	
	
	@Override
	public Screen getInitScreen() 
	{					
		
		
		Graphics g = getGraphics();
		Assets.player = g.newImage("pl.png", ImageFormat.ARGB8888);	
		Assets.background = g.newImage("background.png", ImageFormat.ARGB4444);
		Assets.left = g.newImage("left.png", ImageFormat.ARGB4444);
		Assets.right = g.newImage("right.png", ImageFormat.ARGB4444);
		Assets.leftDown = g.newImage("leftdown.png", ImageFormat.ARGB4444);
		Assets.rightDown = g.newImage("rightdown.png", ImageFormat.ARGB4444);
		Assets.bulletButton = g.newImage("bulletButton.png", ImageFormat.ARGB4444);
		Assets.bulletButtonDown = g.newImage("bulletButtonDown.png", ImageFormat.ARGB4444);
		Assets.clouds = g.newImage("cloud_night.png", ImageFormat.ARGB4444);
		Assets.clouds1 = g.newImage("cloud_night1.png", ImageFormat.ARGB4444);
		Assets.moon = g.newImage("Red-Moon-04.jpg", ImageFormat.ARGB4444);
		Assets.stars = g.newImage("stars.png", ImageFormat.ARGB4444);
		Assets.level1 = g.newImage("level1.png", ImageFormat.ARGB4444);
		Assets.level2 = g.newImage("level2.png", ImageFormat.ARGB4444);
		Assets.Point = g.newImage("nnpoints.png", ImageFormat.ARGB4444);
		Assets.lava = g.newImage("lava.png", ImageFormat.ARGB4444);
		Assets.gameOver = g.newImage("gameOver.png", ImageFormat.ARGB4444);
		Assets.menu = g.newImage("menu.png", ImageFormat.ARGB4444);
		
		Assets.b0 = g.newImage("b1.png", ImageFormat.ARGB4444);
		Assets.b1 = g.newImage("b2.png", ImageFormat.ARGB4444);
		Assets.b2 = g.newImage("b3.png", ImageFormat.ARGB4444);
		Assets.b3 = g.newImage("b4.png", ImageFormat.ARGB4444);
		Assets.b4 = g.newImage("b5.png", ImageFormat.ARGB4444);
		Assets.b5 = g.newImage("b6.png", ImageFormat.ARGB4444);
		
		Assets.load(this);
		
		return  new MenuScreen(this);
	}
	
	public void Update(float deltaTime)
	{
	
		
	}
	
	@Override
	public void onBackPressed() {
		getCurrentScreen().backButton();
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		Assets.bgMusic.play();
	}
	

	@Override
	public void onPause() {
		super.onPause();
		Assets.bgMusic.pause();

	}

}
