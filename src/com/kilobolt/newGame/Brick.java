package com.kilobolt.newGame;

import java.util.ArrayList;

import android.graphics.Point;
import android.graphics.Rect;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class Brick {
	
	int posX, posY, speed, type;
	int width, height;
	int index;
	float reverseTime;
	
	public  ArrayList<Point> brickPos;
	
	Brick(int pX, int pY,int wid, int he, int tipe)
	{
		posX = pX;
		posY = pY;
		type = tipe;
		speed = 2;
		width = wid;
		height = he;
		reverseTime = 0;
		
		brickPos = new ArrayList<Point>();
	}
	
	float t = 0;
	float t1 = 0;
	Rect p,b;
	
	public void Update(float deltaTime, Input input)
	{	
		reverseTime += deltaTime/60;
		
		if(!background.reverseBTDown)
		{
		t += deltaTime/30;
		
		
		b= new Rect(posX, posY, posX + width, posY + height);
		p = new Rect(Player.getInstance().posX,Player.getInstance().posY,Player.getInstance().posX + Player.getInstance().width,
				Player.getInstance().posY + Player.getInstance().height);
		
		if(t > 6.0f)
			posX -= speed;
		
		t1 += deltaTime/60;
		
		if(speed <= 10)
		{
			if(t1 > 4)
			{
				speed += 1;
				t1 = 0;
			}
		}
		
		
		if(p.intersect(b))
		{	
		
			Player.getInstance().posY -= Player.getInstance().gravity;
			Player.getInstance().IsOnGround = true;
			
		}
		
		
		
		if(reverseTime < 5)
		{
			brickPos.add(new Point(posX, posY));
		}
		else
		{
			brickPos.add(new Point(posX, posY));
			brickPos.remove(0);
		}
		
		
		index = brickPos.size() - 1;
		
		}
		else
		{
			if(index > 0)
			{
			posX = brickPos.get(index).x;
			posY = brickPos.get(index).y;
			index--;
			}
		}
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		if(type == 0)
		{
			g.drawImage(Assets.b0, posX, posY);
		}
		else if(type == 1)
		{
			g.drawImage(Assets.b1, posX, posY);
		}
		else if(type == 2)
		{
			g.drawImage(Assets.b2, posX, posY);
		}
		else if(type == 3)
		{
			g.drawImage(Assets.b3, posX, posY);
		}
		else if(type == 4)
		{
			g.drawImage(Assets.b4, posX, posY);
		}
		else 
		{
			g.drawImage(Assets.b5, posX, posY);
		}
	}


}
