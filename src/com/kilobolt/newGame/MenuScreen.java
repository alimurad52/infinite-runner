package com.kilobolt.newGame;

import android.app.Application;
import android.graphics.Color;
import android.graphics.Paint;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class MenuScreen extends Screen {

	
	public MenuScreen(Game game) 
	{
		super(game);		
	}

	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		if(input.isTouchDown(0))
		{
			if(input.getTouchY(0) > 260  && input.getTouchY(0) < 360)
			{
				if(input.getTouchX(0) > 300  && input.getTouchX(0) < 480)
				{
					game.setScreen(new InitialScreen(game));
				}
			}
			
			if(input.getTouchY(0) > 380  && input.getTouchY(0) < 480)
			{
				if(input.getTouchX(0) > 300  && input.getTouchX(0) < 480)
				{
					System.exit(0);
				}
			}
		}
		
		
	}

	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		g.drawImage(Assets.menu, 0, 0);
		
	}
	
	
	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}
}
