package com.kilobolt.newGame;

import java.util.ArrayList;
import java.util.Random;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class ScoreUpsManager
{
	ArrayList<ScoreUps> list;
	Random rand;
	float t = 0;
	
	ScoreUpsManager()
	{
		list = new ArrayList<ScoreUps>();
		rand = new Random();	
		list.add(new ScoreUps(700, 250));
	}
	
	
	public void Update(float deltaTime, Input input)
	{	
		
		if(!background.reverseBTDown)
		{			
			t += deltaTime/60;
			
			if(t > 2)
			{
				list.add(new ScoreUps(800, 200 + rand.nextInt(100)));
				t = 0;
			}
			
			for(int i = 0; i<list.size(); i++)
			{
				list.get(i).Update(deltaTime, input);
			}
			
			
			for(int i = 0; i<list.size(); i++)
			{
				if(list.get(i).isHit)
				{
					Player.getInstance().score += 50;
					list.remove(i);
				}
			}
		}
		
		
		
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		for(int i = 0; i<list.size();i++)
		{
			list.get(i).Paint(deltaTime, g);
		}
	}

}
