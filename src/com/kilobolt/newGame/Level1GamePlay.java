package com.kilobolt.newGame;

import android.graphics.Color;
import android.graphics.Paint;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class Level1GamePlay extends Screen{

	Player player;
	background Background;
	BrickManager  bm;
	Recording recording;
	float t;
	public Level1GamePlay(Game game) 
	{
		super(game);
		
		player = new Player(400,0);
		Background = new background();
		bm = new BrickManager();
		recording = new Recording();
		t = 0;
	}

	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		player.Update(deltaTime,input);
		Background.Update(deltaTime, input);
		bm.Update(deltaTime, input);
		
		if(!background.reverseBTDown)
		{	
			t += deltaTime / 60;
		}
		
		if(t > 90)
		{
			if(Player.getInstance().isDead == false)
			{
				game.setScreen(new Level2InitialScreen(game));
			}
		}
		
		if(Player.getInstance().isDead)
		{
			game.setScreen(new GameOverScreen(game));
		}
	}

	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		Background.Paint(deltaTime, g);
		player.Paint(deltaTime,g);
		bm.Paint(deltaTime, g);
		Paint p = new Paint();
		p.setColor(Color.RED);
		p.setTextSize(25);
		
		g.drawString("Level-1", 20, 70, p);
	}
	
	
	

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}
}
