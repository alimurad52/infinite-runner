package com.kilobolt.newGame;

import com.kilobolt.framework.Image;
import com.kilobolt.framework.Music;
import com.kilobolt.framework.Sound;

public class Assets {
	
	public static Image stars, moon, clouds1, clouds, player,background, left, right, leftDown, rightDown, bulletButton, bulletButtonDown, 
	b0, b1, b2, b3, b4, b5;	
	public static Music bgMusic;
	public static Sound jump, collide;
	public static Image level1, level2, Point, lava, gameOver, menu;
	
	public static void load(SampleGame sampleGame)
	{
		bgMusic = sampleGame.getAudio().createMusic("Music.mp3");
		bgMusic.setLooping(true);
		bgMusic.setVolume(0.5f);
		bgMusic.play();
		
		jump = sampleGame.getAudio().createSound("j.wav");
		collide = sampleGame.getAudio().createSound("spawn-01.wav");
	}
	
}
