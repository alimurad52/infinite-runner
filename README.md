# README #

# Infinite Runner #
The game is developed for android and has the concept of going back in time. The position of the character is recorder is recorder every 5 seconds and the player has 2 seconds after falling to go back in time and save the character.

# Story Line #
The game is of one infinite level where as the player move the speed increases and the player has to touch the screen to jump from one moving tile to another and if the player falls he has 2 seconds to bring back the character to the position within the last 5 seconds.

# License #

```
#!python

    Infinite Runner
    Copyright (C) 2017 Ali Murad

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```